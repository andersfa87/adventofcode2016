using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode
{
    public class Day5
    {
        private const string Input = @"reyedfim";

        public void Run()
        {
            var sb = new StringBuilder();
            var part2 = new char?[8];

            using(var md5 = MD5.Create())
            {
                long counter = 0;
                while(sb.Length < 8 || part2.Count(x => x.HasValue) < 8)
                {
                    var hashBytes = md5.ComputeHash(Encoding.ASCII.GetBytes($"{Input}{counter}"));

                    var hex = BitConverter.ToString(hashBytes).Replace("-", "");
                    //Console.WriteLine(hex.Substring(0, 5));
                    if(hex.Substring(0, 5).Equals("00000"))
                    {
                        if(sb.Length < 8)
                        {
                            sb.Append(hex[5]);
                        }
                        
                        Console.WriteLine($"hex:{hex}, index:{counter}");
                        int position = int.Parse(hex[5].ToString(), NumberStyles.HexNumber);
                        
                        if(position < 8 && position >= 0 && part2[position] == null)
                        {
                            part2[position] = hex[6];
                        }

                    }
                    counter++;
                }
            }

            Console.WriteLine($"Password: {sb.ToString()}");
            Console.WriteLine($"Password: {string.Join("", part2.Select(x => x ?? ' '))}");
        }
    }
}
