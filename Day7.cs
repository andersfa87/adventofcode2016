using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public class Day7
    {

        public void Run()
        {
            var testInput = new string[] {
                "abba[mnop]qrst",
                "abcd[bddb]xyyx",
                "aaaa[qwer]tyui",
                "ioxxoj[asdfgh]zxcvbn"
            };
            var lines = File.ReadAllLines("day7");
            var abbaCount = 0;
            var supportsSSL = 0;
            foreach(var line in lines)
            {
                var supernet = new List<string>();
                var hypernet = new List<string>();

                int endIndex = 0;
                for(var startIndex = line.IndexOf("["); startIndex > -1; startIndex = line.IndexOf("[", startIndex + 1))
                {
                    supernet.Add(line.Substring(endIndex, startIndex - endIndex));
                    endIndex = line.IndexOf("]", startIndex);
                    hypernet.Add(line.Substring(startIndex + 1, (endIndex - startIndex) - 1));
                }
                supernet.Add(line.Substring(endIndex + 1, (line.Length - endIndex)-1));

                if(supernet.Any(IsAbba) && !hypernet.Any(IsAbba))
                {
                    abbaCount++;
                }

                foreach(var v in supernet)
                {
                    for(var i = 0; i < v.Length - 2; i++)
                    {
                        char a = v[i], b = v[i+1], c = v[i+2];
                        if(a == c && a != b)
                        {
                            if(hypernet.Any(h => IsBAB(h, string.Concat(a, b, c))))
                            {
                                supportsSSL++;
                                break;
                            }
                        }
                    }
                }
            }
            Console.WriteLine(abbaCount);
            Console.WriteLine(supportsSSL);
        }

        private bool IsAbba(string value)
        {
            for(var i = 0; i < value.Length - 3; i++)
            {
                char a = value[i], b = value[i+1], c = value[i+2], d = value[i+3];
                if(a == d && b == c && a != b && c != d)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsBAB(string hypernet, string aba)
        {
            for(var i = 0; i < hypernet.Length - 2; i++)
            {
                //bab
                char a1 = hypernet[i], b1 = hypernet[i+1], c1 = hypernet[i+2];
                
                //aba
                char a2 = aba[0], b2 = aba[1], c2 = aba[2];
                
                if(a1 == b2 && b1 == a2 && c1 == b2 && b1 == c2)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
