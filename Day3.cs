using System;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public class Day3
    {

        public void Run()
        {
            using(var fs = new FileStream("day3", FileMode.Open, FileAccess.Read))
            using(var reader = new StreamReader(fs))
            {
                var content = reader.ReadToEnd().Replace("\r", "");
                var lines = content.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                
                var validTriangles = lines.Count(line => {
                    var values = line.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray();
                    return IsTriangleValid(values[0], values[1], values[2]);
                });

                Console.WriteLine($"Valid triangles: {validTriangles}");
            } 
        }

        public void Run2()
        {
            using(var fs = new FileStream("day3", FileMode.Open, FileAccess.Read))
            using(var reader = new StreamReader(fs))
            {
                var content = reader.ReadToEnd().Replace("\r", "");
                var lines = content.Split(new char[]{'\n'}, StringSplitOptions.RemoveEmptyEntries);
                
                var validTriangles = 0;

                for(var i = 0; i < lines.Length; i += 3)
                {
                    var line1 = lines[i].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray();
                    var line2 = lines[i+1].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray();
                    var line3 = lines[i+2].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray();

                    if(IsTriangleValid(line1[0], line2[0], line3[0])) validTriangles++;
                    if(IsTriangleValid(line1[1], line2[1], line3[1])) validTriangles++;
                    if(IsTriangleValid(line1[2], line2[2], line3[2])) validTriangles++;
                }

                Console.WriteLine($"Valid triangles: {validTriangles}");
            } 
        }

        private bool IsTriangleValid(int a, int b, int c)
        {
            return a + b > c && b + c > a && a + c > b;
        }

    }
}
