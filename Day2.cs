using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public class Day2
    {
        private const string TestInput = @"ULL
RRDDD
LURDL
UUUUD";

        private const string Input = @"UULLULLUULLLURDLDUURRDRRLDURDULLRURDUDULLLUULURURLRDRRRRULDRUULLLLUUDURDULDRRDRUDLRRLDLUDLDDRURURUURRRDDDLLRUDURDULUULLRRULLRULDUDRDRLDLURURUDDUDLURUDUDURLURURRURLUDDRURRDLUURLLRURRDUDLULULUDULDLLRRRDLRDLDUDRDDDRRUURRRRRUURRDRRDLURDRRURDLLUULULLRURDLDDDRRLLRRUURULURUUDDLRRUDDRURUUDLRLRDLRURRRDULLDLRUDDUULRDULURUURDULUDLLRRLDDLRDLRUDRLDDRLRRRDURDULLRRRDRRLUURURDRRDRRLDLUDURURLDUURDRUDRDDRLDRRLDLURURULLUURUDUUDLRLL
LLLULLULDDULRLLURLLLRUUDDLRUULRLULLDLLRRDRLRLRLLDRUUURULDRDDLUDLLDUDULLLRLULLLRULDRDRUDLLRLRLLUDULRRRLDRUULDDULLDULULLUDUDLDRDURDLDLLDUDRRRDLUURRUURULLURLDURLRRLLDDUUULDRLUUDUDLURLULUDURRDRLLDDDDDRRULLRLDULULDDRUURRDLUDDDUDURDDRDRULULLLLUURDURUUUULUDLRURRULRDDRURURLLRLUUDUUURDLLDDLUDRLLLUDLLLLULRLURDRRRDUUDLLDLDDDURRDDRURUURDDRURRLDDDURDLLUURUUULRLUURRUDRLLDLURDUDRLULDLRLULULUDDLRDUDRUDLUULUULDURDRRRRLRULLUDRDDRDLDUDRDRRLDLLLLUDDLRULDLLDDUULDDRRULRRUURUDRDURLLLDDUUDRUUDLULLDR
UDUUULLDDDDLUDLDULRLRDLULLDDRULDURRLURRUDLRRUDURRDUDRRRUULRLLRLUDLDRRDUURDDRDRDUUUDUDLDLLRRLUURLUUUDDDUURLULURRLURRRDRDURURUDRLRUURUDRUDDDRDRDLDRDURDLDRRDUUDLLURLDDURRRLULDRDRLLRLLLRURLDURDRLDRUURRLDLDRLDDDRLDLRLDURURLLLLDDRDUDLRULULLRDDLLUDRDRRLUUULDRLDURURDUDURLLDRRDUULDUUDLLDDRUUULRRULDDUDRDRLRULUUDUURULLDLLURLRRLDDDLLDRRDDRLDDLURRUDURULUDLLLDUDDLDLDLRUDUDRDUDDLDDLDULURDDUDRRUUURLDUURULLRLULUURLLLLDUUDURUUDUULULDRULRLRDULDLLURDLRUUUDDURLLLLDUDRLUUDUDRRURURRDRDDRULDLRLURDLLRRDRUUUURLDRURDUUDLDURUDDLRDDDDURRLRLUDRRDDURDDRLDDLLRR
ULDRUDURUDULLUDUDURLDLLRRULRRULRUDLULLLDRULLDURUULDDURDUUDLRDRUDUDDLDRDLUULRRDLRUULULUUUDUUDDRDRLLULLRRDLRRLUDRLULLUUUUURRDURLLRURRULLLRLURRULRDUURRLDDRRDRLULDDRRDRLULLRDLRRURUDURULRLUDRUDLUDDDUDUDDUDLLRDLLDRURULUDRLRRULRDDDDDRLDLRRLUUDLUURRDURRDLDLDUDRLULLULRLDRDUDLRULLULLRLDDRURLLLRLDDDLLLRURDDDLLUDLDLRLUULLLRULDRRDUDLRRDDULRLLDUURLLLLLDRULDRLLLUURDURRULURLDDLRRUDULUURRLULRDRDDLULULRRURLDLRRRUDURURDURDULURULLRLDD
DURLRRRDRULDLULUDULUURURRLULUDLURURDDURULLRRUUDLRURLDLRUDULDLLRRULLLLRRLRUULDLDLLRDUDLLRLULRLLUUULULRDLDLRRURLUDDRRLUUDDRRUDDRRURLRRULLDDULLLURRULUDLRRRURRULRLLLRULLRRURDRLURULLDULRLLLULLRLRLLLDRRRRDDDDDDULUUDUDULRURDRUDRLUULURDURLURRDRRRRDRRLLLLUDLRRDURURLLULUDDLRLRLRRUURLLURLDUULLRRDURRULRULURLLLRLUURRULLLURDDDRURDUDDULLRULUUUDDRURUUDUURURRDRURDUDRLLRRULURUDLDURLDLRRRRLLUURRLULDDDUUUURUULDLDRLDUDULDRRULDRDULURRUURDU";

        public void Run()
        {
            var numpad = new int[3,3] {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };

            var sequence = new List<string>();

            int currentX = 1, currentY = 1;

            foreach(var line in Input.Split(new char[]{'\n'}))
            {
                foreach(char action in line)
                {
                    switch(action)
                    {
                        case 'R':
                            if(currentX + 1 < 3)
                            {
                                currentX++;
                                
                            }
                        break;
                        case 'D':
                            if(currentY + 1 < 3)
                            {
                                currentY++;
                            }
                        break;
                        case 'L':
                            if(currentX - 1 >= 0)
                            {
                                currentX--;
                            }
                        break;
                        case 'U':
                            if(currentY - 1 >= 0)
                            {
                                currentY--;
                            }                    
                        break;                 
                    }
                }
                sequence.Add(numpad[currentY, currentX].ToString());
            }

            var key = numpad[1,1];
            Console.Write(sequence.Aggregate((left, right) => $"{left}{right}"));
        }

        public void RunPart2() 
        {
            var numpad = new char?[5,5] {
                {null, null, '1' , null, null},
                {null, '2', '3' , '4', null},
                {'5', '6', '7' , '8', '9'},
                {null, 'A', 'B' , 'C', null},
                {null, null, 'D' , null, null}
            };

            var sequence = new List<string>();

            int currentX = 0, currentY = 2;

            foreach(var line in Input.Split(new char[]{'\n'}))
            {
                foreach(char action in line)
                {
                    switch(action)
                    {
                        case 'R':
                            if(currentX + 1 < numpad.GetLength(0) && numpad[currentY, currentX + 1].HasValue)
                            {
                                currentX++;
                            }
                        break;
                        case 'D':
                            if(currentY + 1 < numpad.GetLength(1) && numpad[currentY + 1, currentX].HasValue)
                            {
                                currentY++;
                            }
                        break;
                        case 'L':
                            if(currentX - 1 >= 0 && numpad[currentY, currentX - 1].HasValue)
                            {
                                currentX--;
                            }
                        break;
                        case 'U':
                            if(currentY - 1 >= 0 && numpad[currentY - 1, currentX].HasValue)
                            {
                                currentY--;
                            }                  
                        break;                 
                    }
                }
                sequence.Add(numpad[currentY, currentX].ToString());
            }
            var key = numpad[1,1];
            Console.Write(sequence.Aggregate((left, right) => $"{left}{right}"));
        }
    }
}
