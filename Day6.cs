using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode
{
    public class Day6
    {
        private const string TestInput = @"eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar";

        public void Run()
        {
            //var lines = TestInput.Split(new char[]{'\n'});
            var lines = File.ReadAllLines("day6");
            var columns = new string[lines.First().Length];

            foreach(var line in lines)
            {
                for(var i = 0; i < line.Length; i++) {
                    if(columns[i] == null) columns[i] = "";
                    columns[i] = columns[i] + line[i];
                }
            }

            var message = new StringBuilder();
            var leastCommonMessage = new StringBuilder();

            foreach(var col in columns)
            {
                var mostCommonChar = col.ToCharArray().GroupBy(x => x).Select(x => new { Key = x.Key, Count = x.Count()}).OrderByDescending(x => x.Count).Select(x => x.Key).First();
                var leastCommonChar = col.ToCharArray().GroupBy(x => x).Select(x => new { Key = x.Key, Count = x.Count()}).OrderBy(x => x.Count).Select(x => x.Key).First();
                message.Append(mostCommonChar);
                leastCommonMessage.Append(leastCommonChar);
            }

            Console.WriteLine(message);
            Console.WriteLine(leastCommonMessage);
        }
    }
}
