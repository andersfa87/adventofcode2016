using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public class Day9
    {
        //6+7+9+11+6+18 = 57
        private const string Test = 
@"ADVENTA(1x5)BC(3x3)XYZA(2x2)BCD(2x2)EFG(6x1)(1x3)AX(8x2)(3x3)ABCY";
        private Dictionary<string, long> Cache = new Dictionary<string, long>();

        public void Run()
        {
            var input = Test;
            input = File.ReadAllText("day9");
            var decompressedLength = DecompressString(input);
            Console.WriteLine(decompressedLength);
        }

        public long DecompressString(string data)
        {
            if(Cache.ContainsKey(data))
            {
                return Cache[data];
            }
            var end = 0;
            var i = data.IndexOf("(");
            long decompressedLength = 0;

            while(i > -1)
            {
                //decompressedLength += data.Substring(end, i - end).Length;
                
                var endParanthesis = data.IndexOf(")", i) + 1;
                
                var match = Regex.Match(data.Substring(i, endParanthesis - i), @"(\d+)x(\d+)");
                int take = int.Parse(match.Groups[1].Value);
                int repeat = int.Parse(match.Groups[2].Value);

                for(var j = 0; j < repeat; j++)
                {
                    decompressedLength += DecompressString(data.Substring(endParanthesis, take));
                }
                
                end = endParanthesis + take;
                i = data.IndexOf("(", end);
            }
            decompressedLength += data.Substring(end, data.Length - end).Length;
            Cache[data] = decompressedLength;
            return decompressedLength;
        }
    }
}
