using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public class Pixel
    {
        public bool On { get; set; } = false;
    }

    public class Day8
    {
        private const string Test = 
@"rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1
";

        public void Run()
        {
            var display = new Pixel[6,50];
            for(var y = 0; y < display.GetLength(0); y++)
            {
                for(var x = 0; x < display.GetLength(1); x++)
                {
                    display[y,x] = new Pixel();
                }
            }

            var lines = File.ReadAllLines("day8");

            foreach(var line in lines)
            {
                if(line.StartsWith("rect"))
                {
                    var match = Regex.Match(line, @"(\d+)x(\d+)");
                    var width = int.Parse(match.Groups[1].Value);
                    var height = int.Parse(match.Groups[2].Value);
                    for(var y = 0; y < height; y++)
                    {
                        for(var x = 0; x < width; x++)
                        {
                            display[y,x].On = true;
                        }
                    }
                }
                else if(line.StartsWith("rotate column"))
                {
                    var match = Regex.Match(line, @"x=(\d+) by (\d+)");
                    var x = int.Parse(match.Groups[1].Value);
                    var by = int.Parse(match.Groups[2].Value);
                    RotateColumn(display, x, by);
                }
                else if(line.StartsWith("rotate row"))
                {
                    var match = Regex.Match(line, @"y=(\d+) by (\d+)");
                    var y = int.Parse(match.Groups[1].Value);
                    var by = int.Parse(match.Groups[2].Value);
                    RotateRow(display, y, by);
                }
            }

            Draw(display);
        }

        public void RotateColumn(Pixel[,] source, int x, int by)
        {
            for(var i = 0; i < by; i++)
            {
                Pixel putInCurrent = source[source.GetLength(0)-1,x];
                for(var y = 0; y < source.GetLength(0); y++)
                {
                    var temp = source[y,x];
                    source[y,x] = putInCurrent;
                    putInCurrent = temp;
                }
            }
        }

        public void RotateRow(Pixel[,] source, int y, int by)
        {
            for(var i = 0; i < by; i++)
            {
                Pixel putInCurrent = source[y,source.GetLength(1)-1];
                for(var x = 0; x < source.GetLength(1); x++)
                {
                    var temp = source[y,x];
                    source[y,x] = putInCurrent;
                    putInCurrent = temp;
                }
            }
        }

        public void Draw(Pixel[,] display) 
        {
            int pixelsOn = 0;
            for(var y = 0; y < display.GetLength(0); y++)
            {
                for(var x = 0; x < display.GetLength(1); x++)
                {
                    var pixel = display[y,x];
                    if(pixel.On)
                    {
                        Console.Write ("#");
                        pixelsOn++;
                    }
                    else{
                        Console.Write (" ");
                    }
                }
                Console.Write("\n");
            }
            Console.WriteLine($"Pixels on: {pixelsOn}");
        }
    }
}
