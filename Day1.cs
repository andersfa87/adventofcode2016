using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public enum Facing {
        North,
        West,
        South,
        East
    }

    public static class CircularLinkedList {
        public static LinkedListNode<T> NextOrFirst<T>(this LinkedListNode<T> current)
        {
            return current.Next ?? current.List.First;
        }

        public static LinkedListNode<T> PreviousOrLast<T>(this LinkedListNode<T> current)
        {
            return current.Previous ?? current.List.Last;
        }
    }

    public class Day1
    {
        private const string Input = @"R4, R3, L3, L2, L1, R1, L1, R2, R3, L5, L5, R4, L4, R2, R4, L3, R3, L3, R3, R4, R2, L1, R2, L3, L2, L1, R3, R5, L1, L4, R2, L4, R3, R1, R2, L5, R2, L189, R5, L5, R52, R3, L1, R4, R5, R1, R4, L1, L3, R2, L2, L3, R4, R3, L2, L5, R4, R5, L2, R2, L1, L3, R3, L4, R4, R5, L1, L1, R3, L5, L2, R76, R2, R2, L1, L3, R189, L3, L4, L1, L3, R5, R4, L1, R1, L1, L1, R2, L4, R2, L5, L5, L5, R2, L4, L5, R4, R4, R5, L5, R3, L1, L3, L1, L1, L3, L4, R5, L3, R5, R3, R3, L5, L5, R3, R4, L3, R3, R1, R3, R2, R2, L1, R1, L3, L3, L3, L1, R2, L1, R4, R4, L1, L1, R3, R3, R4, R1, L5, L2, R2, R3, R2, L3, R4, L5, R1, R4, R5, R4, L4, R1, L3, R1, R3, L2, L3, R1, L2, R3, L3, L1, L3, R4, L4, L5, R3, R5, R4, R1, L2, R3, R5, L5, L4, L1, L1";
        
        private const string TestInput = @"R8, R4, R4, R8";

        public void Run()
        {
            var facingState = new LinkedList<Facing>();
            var facing = facingState.AddFirst(Facing.North);
            var last = facingState.AddAfter(facingState.AddAfter(facingState.AddAfter(facing, Facing.East), Facing.South), Facing.West);
            
            int x = 0, y = 0;

            var commands = Input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(c => c.Trim());
            var positions = new HashSet<string>();

            foreach(var command in commands)
            {
                var turn = command.Substring(0, 1);
                var length = int.Parse(command.Substring(1));

                if(turn.Equals("L"))
                {
                    facing = facing.PreviousOrLast();
                }
                else
                {
                    facing = facing.NextOrFirst();
                }
                int previousX = x, previousY = y;
                switch(facing.Value)
                {
                    case Facing.North:
                        y+=length;
                        for(var y2 = previousY; y2 < y; y2++)
                        {
                            CheckPosition(x, y2, positions);
                        }
                        break;
                    case Facing.West:
                        x-=length;
                        for(var x2 = previousX; x2 > x; x2--)
                        {
                            CheckPosition(x2, y, positions);
                        }
                        break;
                    case Facing.South:
                        y-=length;
                        for(var y2 = previousY; y2 > y; y2--) {
                            CheckPosition(x, y2, positions);
                        }
                        break;
                    case Facing.East:
                        x+=length;
                        for(var x2 = previousX; x2 < x; x2++)
                        {
                            CheckPosition(x2, y, positions);
                        }
                        break;
                }
            }

            Console.WriteLine($"End position is x:{x}, y:{y}");
            Console.WriteLine($"Final destination: {Math.Abs(x) + Math.Abs(y)}");

        }

        private void CheckPosition(int x, int y, HashSet<string> positions)
        {
            //Console.WriteLine($"Checking: {x} , {y}");
            if(!positions.Add($"{x}-{y}"))
            {
                Console.WriteLine($"Visited Twice: Checking: {x} , {y}");
                Console.WriteLine($"Visited Twice: {Math.Abs(x) + Math.Abs(y)}");
            }
        }
    }
}
