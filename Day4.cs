using System;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public class Day4
    {

        public void Run()
        {
            var lines = File.ReadAllLines("day4");
            var alphabet = "abcdefghijklmnopqrstuvwxyz";
            var sum = 0;
            foreach(var line in lines)
            {
                var start = line.LastIndexOf("-") + 1;
                var count = line.IndexOf("[") - start;
                var sectorId = int.Parse(line.Substring(start, count));

                var name = line.Substring(0, start- 1);

                var top5 = name.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries)
                               .Aggregate((left, right) => left+right)
                               .ToCharArray()
                               .GroupBy(x => x)
                               .Select(x => new { key = x.Key, count = x.Count() })
                               .OrderByDescending(x => x.count)
                               .ThenBy(x => x.key)
                               .Take(5);

                var checksumStart = line.IndexOf("[") + 1;
                var checksum = line.Substring(checksumStart, line.Length - checksumStart - 1);
                
                var isReal = top5.All(x => checksum.Contains(x.key));

                if(isReal) 
                {
                    sum += sectorId;
                }
                //sectorId = 343;
                var result = "";
                var iterations = sectorId % alphabet.Length;
                foreach(var c in name)
                {
                    if (c == '-') result += " ";
                    else result += alphabet[(alphabet.IndexOf(c) + iterations) % alphabet.Length];
                }

                if(result.Contains("north"))
                {
                    Console.WriteLine(sectorId);
                }
            }
            
            Console.WriteLine(sum);

        }

    }
}
